<?php
  $db = new PDO("mysql:host=localhost;dbname=isw613_library;","isw613_usr","secret");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $stmt1 = $db->prepare("SELECT * FROM borrowers;");
  $stmt1->execute();

  $stmt2 = $db->prepare("SELECT * FROM books;");
  $stmt2->execute();
  //var_dump(  $row['name']);
  //exit;
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Rental</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="col-sm-12">
      <h2>Rent a book</h2>
    </div>


    <div class="panel panel-default">
      <form class="" action="logic/update.php" method="post">
        <div class="col-sm-12 col-md-12">
          <div class="col-sm-3 col-md-3">
            <label class="label label-default" for="name">Seleccione un nombre</label>
            <br>
            <select name="name">
              <?php while ($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {?>
              <option id="<?php echo $row['borrowerid']; ?>" name = "name" value="<?php echo $row['borrowerid']; ?>"> <?php echo $row['name']; ?> </option>
              <?php } ?>
            </select>
          </div>
          <div class="col-sm-3 col-md-3">
            <label class="label label-default" for="title">Seleccione un Libro</label>
            <br>
            <select name="title">
              <?php while ($row = $stmt2->fetch(PDO::FETCH_ASSOC)) {?>
                <option id="<?php echo $row['bookid']; ?>" name = "title" value="<?php echo $row['bookid']; ?>"> <?php echo $row['title']; ?> </option>
              <?php } ?>
            </select>
          </div>
          <div class="col-sm-3 col-md-3">
            <label class="label label-default" for="title">Duedate</label>
            <br>
            <input type="date" name="date" step="1" min="" max="" value="2017-11-24">
          </div>
          <button class="btn btn-default" type="submit" name="button">Rent</button>
        </div>
      </form>
    </div>




  </body>
</html>
