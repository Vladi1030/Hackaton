<?php
  $db = new PDO("mysql:host=localhost;dbname=isw613_library;","isw613_usr","secret");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // when binding parameters you must pass a variable by reference
  // you cannot bind a literal
  $stmt = $db->prepare("SELECT books.bookid, books.title, books.author, books.duedate,
    DATEDIFF(CURDATE(), books.duedate) AS delay, borrowers.name FROM books
    INNER JOIN borrowers ON books.borrowerid=borrowers.borrowerid;");
  $stmt->execute();
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Result</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <div style="width:70%; float: left;">
        <h2>Books Table</h2>
        <br>
      </div>
      <form style="width:25%; float:right; margin-top:3%;" class="" action="main.php" method="post">
        <input class="btn btn-primary" type="submit" name="" value="Back">
      </form>
      <table class="table">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Author</th>
            <th>Borrower Name</th>
            <th>Expiration Date</th>
            <th>Delay's days</th>
            <th>Return back</th>
          </tr>
        </thead>
        <tbody>
          <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {?>
            <tr>
              <td></td>
              <td><?php echo $row['title']; ?></td>
              <td><?php echo $row['author']; ?></td>
              <td><?php echo $row['name']; ?></td>
              <td><?php echo $row['duedate']; ?></td>
              <?php if ($row['delay'] > 0): ?>
                <td><?php echo $row['delay']; ?></td>
              <?php else: ?>
                <td>0</td>
              <?php endif; ?>
              <td>
                <form class="" action="return_book.php" method="post">

                  <button class="btn btn-primary" type="submit" value="<?php echo $row['bookid']; ?>" name="return_button"> --> </button>
                </form>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </body>
</html>
