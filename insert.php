<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>DataBase</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  </head>
  <body>
    <form name="add" style="width: 25%; margin-left:37%; margin-top:10%;" class="" action="add.php" method="post">
      <h2 style="margin-left:18%;">Add a new book</h2>
      <br>
      <div class="form-group">
        <label for="nameI">Name</label>
        <input id="nameI" type="text" class="form-control" name="nameI" value="">
      </div>
      <div class="form-group">
        <label for="authorI">Author</label>
        <input id="authorI" class="form-control" type="text" name="authorI" value="">
      </div>
      <br>
      <br>
      <input class="btn btn-primary" type="submit" name="" value="Add">
      <form class="" action="index.php" method="post">
        <a class="btn btn-primary" href="main.php">Back</a>
      </form>
    </form>
  </body>
</html>
