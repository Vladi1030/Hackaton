<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Main menu</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div style="margin:10%;margin-left:33%;">
      <div class="container">
        <div class="col-md-12" style="width:70%; float: left;">
          <h2>This is the world book</h2>
          <br>
          <div class="form-group" class="col-md-12">
            <label for="nameI">Do you want to look for a book?</label>
            <br>
            <a class="btn btn-primary" href="search.php">Search</a>
          </div>
          <div class="form-group" class="col-md-12">
            <label for="authorI">Or do you prefer to insert a book?</label>
            <br>
            <a class="btn btn-primary" href="insert.php">Insert</a>
          </div>
          <div class="form-group" class="col-md-12">
            <label for="authorI">If not why you do not try to rent a book?</label>
            <br>
            <a class="btn btn-primary" href="rental.php">Rent</a>
          </div>
          <div class="form-group" class="col-md-12">
            <label for="authorI">Do you want to see the whole rented books list </label>
            <br>
            <a class="btn btn-primary" href="onloan_books.php">See</a>
          </div>
      </div>
    </div>
  </body>
</html>
