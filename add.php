<?php
    $searchtitle = $_POST['nameI'];
    $searchauthor = $_POST['authorI'];

    $db = new PDO("mysql:host=localhost;dbname=isw613_library;","isw613_usr","secret");
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // when binding parameters you must pass a variable by reference
    // you cannot bind a literal
    $stmt = $db->prepare("insert into books(title, author) values(:title, :author);");
    $stmt->execute(array(":title"=>"$searchtitle",
                         ":author"=>"$searchauthor"));
    // echo '<script language="javascript">alert("Book added");</script>';
    header("Location: main.php");
?>
