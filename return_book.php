<?php
  $bookid = $_POST['return_button'];

  $db = new PDO("mysql:host=localhost;dbname=isw613_library;","isw613_usr","secret");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  //--------------------------------------------------------------------------------------
  $stmt1 = $db->prepare("UPDATE books SET onloan = 0, duedate = NULL, borrowerid = NULL WHERE bookid = :bookidvalue");
  $stmt1->execute(array(":bookidvalue"=>"$bookid"));
  
  header("Location: onloan_books.php");
?>
