<?php
  $searchtitle = $_POST['nameI'];
  $searchauthor = $_POST['authorI'];

  $db = new PDO("mysql:host=localhost;dbname=isw613_library;","isw613_usr","secret");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // v3
  $stmt = $db->prepare("select * from books where title regexp ? " .
                       "and author regexp ?");

  // when binding parameters you must pass a variable by reference
  // you cannot bind a literal
  $stmt = $db->prepare("select * from books where title like :title and author like :author");
  $stmt->execute(array(":title"=>"%$searchtitle%",
                       ":author"=>"%$searchauthor%"));
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Result</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <div style="width:70%; float: left;">
        <h2>Books Table</h2>
        <p>This is the result of your parameters</p>
      </div>
      <form style="width:25%; float:right; margin-top:3%;" class="" action=" ../search.php" method="post">
        <input class="btn btn-primary" type="submit" name="" value="Back">
      </form>
      <table class="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Author</th>
          </tr>
        </thead>
        <tbody>
          <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {?>
            <tr>
              <td><?php echo $row['title']; ?></td>
              <td><?php echo $row['author']; ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </body>
</html>
