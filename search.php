<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>DataBase</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  </head>
  <body>
    <form name="search" style="width: 25%; margin-left:37%; margin-top:10%;" class="" action="logic/result.php" method="post">
      <h2 style="margin-left:30%;">Find Books</h2>
      <br>
      <div class="form-group">
        <label for="nameI">Name</label>
        <input id="nameI" type="text" class="form-control" name="nameI" value="">
      </div>
      <div class="form-group">
        <label for="authorI">Author</label>
        <input id="authorI" class="form-control" type="text" name="authorI" value="">
      </div>
      <a href="insert.php">New Book</a>
      <br>
      <br>
      <input class="btn btn-primary" type="submit" name="" value="Search">
      <a class="btn btn-primary" href="main.php">Back</a>
    </form>
  </body>
</html>
